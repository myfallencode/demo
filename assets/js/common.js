$(function(){
   $('.navigation-menu__item-hamburger').on("click", function(){
     if(!$(this).data().opened){
         $(this).data().opened = true;
         $(this).addClass('navigation-menu__hamburger--rotate');
         $('.navigation-menu__item').addClass('navigation-menu__item--hidden');
     }else{
         $(this).data().opened = false;
         $(this).removeClass('navigation-menu__hamburger--rotate');
         $('.navigation-menu__item').removeClass('navigation-menu__item--hidden');
     }
   });
});